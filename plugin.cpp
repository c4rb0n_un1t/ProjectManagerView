#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_guiElementBase(new GUIElementBase(this, {"MainMenuItem"}, "qrc:/form.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IGUIElement), m_guiElementBase}
	},
	{
		{INTERFACE(IProjectManager), m_projectManager},
		{INTERFACE(IDevPluginInfoDataExtention), m_devPluginInfo},
		{INTERFACE(IDevProjectManagerDataExtention), m_devProjectManager},
	});

	m_guiElementBase->initGUIElementBase(
	{
		{"projectManager", m_projectManager.data()},
		{"devPluginInfo", m_devPluginInfo.data()},
		{"devProjectManager", m_devProjectManager.data()},
	}
	);
}

Plugin::~Plugin()
{
}
