import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1

Item {
    id: root
    property var projectManagerObject: projectManager.object
    property var devPluginInfoObject: devPluginInfo.object
    property var devPluginInfoModel: devPluginInfoObject.model
    property var devProjectManagerObject: devProjectManager.object
    property var devProjectManagerModel: devProjectManagerObject.model
    property real ratio: 1

    Rectangle {
        anchors.fill: parent
        color: "#2e3440"
    }
    ListModel {
        id: emptyModel
        ListElement {
            name: "test"
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10 * ratio

        Button {
            id: exitButton
            text: "Back"
            font.pixelSize: 18 * ratio
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            width: 60 * ratio
            height: width
            onClicked: uiElement.closeSelf()
            background: Rectangle {
                color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
                radius: 90
            }
            Component.onCompleted: {
                fileDialog.accepted.connect(fileSelected)
            }
            function fileSelected() {
                projectManagerObject.projectFilePath = fileDialog.currentFile
            }
        }
        RowLayout {
            TextField {
                id: label
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                text: projectManagerObject.projectFilePath
                font.pixelSize: 20 * ratio
                readOnly: true
            }

            Button {
                id: selectProjectButton
                text: "Select project file"
                font.pixelSize: 18 * ratio
                anchors.margins: 10 * ratio
                width: 60 * ratio
                height: width
                onClicked: fileDialog.open()
                background: Rectangle {
                    color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
                    radius: 90
                }
                Component.onCompleted: {
                    fileDialog.accepted.connect(fileSelected)
                }
                function fileSelected() {
                    projectManagerObject.projectFilePath = fileDialog.currentFile
                }
                FileDialog {
                    id: fileDialog
                    folder: StandardPaths.writableLocation(
                                StandardPaths.DocumentsLocation)
                }
            }
        }

        ListView {
            id: listView
            spacing: 4 * ratio
            model: projectManagerObject !== null
                   && projectManagerObject !== undefined ? devPluginInfoObject.model : emptyModel
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true
            delegate: Rectangle {
                width: parent.width
                height: 50 * ratio
                color: "#4c566a"
                RowLayout {
                    anchors.fill: parent
                    spacing: 0
                    Label {
                        id: nameText
                        text: model.name
                        Layout.preferredWidth: parent.width / 2
                    }
                    Button {
                        Layout.preferredWidth: parent.width / 7
                        Layout.fillHeight: true
                        background: Rectangle {
                            color: parent.hovered ? "#88c0d0" : (model.isEnabled ? "#a3be8c" : "#d8dee9")
                        }
                        text: model.isEnabled ? (hovered ? "Disable" : "Enabled") : (hovered ? "Enable" : "Disabled")
                        onPressed: {
                            model.isEnabled = !model.isEnabled
                        }
                        contentItem: Text {
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Text.Wrap
                            text: parent.text
                            color: "#3b4252"
                        }
                    }
                    Button {
                        Layout.preferredWidth: parent.width / 7
                        Layout.fillHeight: true
                        background: Rectangle {
                            color: model.updateAvailable ? (parent.hovered ? "#88c0d0" : "#a3be8c") : "#d8dee9"
                        }
                        text: model.updateAvailable ? (hovered ? "Run update" : "Update available") : "No updates"
                        onPressed: {
                            if (model.updateAvailable)
                                model.updateAvailable = !model.updateAvailable
                        }
                        contentItem: Text {
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Text.Wrap
                            text: parent.text
                            color: "#3b4252"
                        }
                    }
                    Button {
                        Layout.preferredWidth: parent.width / 7
                        Layout.fillHeight: true
                        background: Rectangle {
                            color: model.hasLocalChanges ? (parent.hovered ? "#88c0d0" : "#a3be8c") : "#d8dee9"
                        }
                        text: model.hasLocalChanges ? (hovered ? "Commit" : "Has local changes") : "No local changes"
                        onPressed: {
                            if (model.hasLocalChanges)
                                model.hasLocalChanges = !model.hasLocalChanges
                        }
                        contentItem: Text {
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Text.Wrap
                            text: parent.text
                            color: "#3b4252"
                        }
                    }
                }
            }
        }

        RowLayout {
            Button {
                id: refreshButton
                text: "Refresh plugins"
                font.pixelSize: 18 * ratio
                Layout.alignment: Qt.AlignRight
                width: 60 * ratio
                height: width
                onClicked: projectManagerObject.refreshExistingProjects()
                background: Rectangle {
                    color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
                    radius: 90
                }
            }
        }
    }
}
